# Jaeger-auto

Small wrapper around @risingstack/opentracing-auto and jaeger-client

## Why

Extreme lazyness.

## Example

```javascript
'use strict'
const { initTracer } = require('jaeger-auto')
initTracer('awesome-service', {
  type: 'service',
  environment: process.env.NODE_ENV,
  'awesome-service.version': "some-git-hash"
})

const express = require('express')


// Somewhere else in your code
const { getTracer } = require('jaeger-auto')
const tracer = getTracer()

function soemthingImportant() {
  // Let's add a child span to the current tracing context
  let span = tracer.startChildSpan(tracer, 'something_important')
  asyncFunction((err, result) => {
    if (err) {
      span.setTag("error",true) 
    }
    span.finish()
  })

}
```