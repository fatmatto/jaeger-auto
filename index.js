'use strict'

const jaeger = require('jaeger-client')
const UDPSender = require('jaeger-client/dist/src/reporters/udp_sender').default
const cls = require('@risingstack/opentracing-auto/src/cls')
const Instrument = require('@risingstack/opentracing-auto')

// Singleton tracer instance
let __tracerInstance = null

/**
 *
 * @returns THe singleton Tracer instance
 * @throws Error if this is called before initTracer()
 */
function getTracer () {
  if (__tracerInstance) {
    return __tracerInstance
  } else {
    throw new Error('Tracer must be initialized with initTracer(serviceName,tags) first')
  }
}

/**
 * Creates an (singleton) instance of a Tracer
 * @param {String} serviceName
 * @param {Object} tags
 * @param {Object} options
 */
function initTracer (
  serviceName,
  tags = {},
  options = {}) {
  options.reporter = options.reporter || {}
  // Default to local jaeger-reporter
  options.reporter.host = options.reporter.host || 'localhost'
  options.reporter.port = options.reporter.port || 6832

  if (!serviceName || typeof serviceName !== 'string') { throw new Error('serviceName is required and must be a string') }

  if (typeof tags !== 'object') { throw new Error('tags must be an object') }

  const sampler = new jaeger.ConstSampler(true)
  const reporter = new jaeger.RemoteReporter(new UDPSender(options.reporter))
  const tracer = new jaeger.Tracer(serviceName, reporter, sampler, {
    tags: tags
  })
  // eslint-disable-next-line
  const instrument = new Instrument({ tracers: [tracer], httpTimings: true })

  __tracerInstance = tracer

  __tracerInstance.getRootSpan = cls.getRootSpan
  __tracerInstance.startChildSpan = cls.startChildSpan

  return __tracerInstance
}

module.exports = { getTracer, initTracer }
