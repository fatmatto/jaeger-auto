const { initTracer, getTracer } = require('../index')
initTracer('rest-service', {
  type: 'service'
})
const apiCompatibilityChecks = require('../node_modules/opentracing/lib/test/api_compatibility.js').default
apiCompatibilityChecks(() => getTracer())
